const flatten = require("../flattenArray")
const nestedArray = [1, [2], [[3]], [[[4]]]]
const expectedResult = [1, 2, 3, 4]

const result = flatten(nestedArray)
console.log(result)

if(JSON.stringify(result)==JSON.stringify(expectedResult)){
    console.log("testPassed")
}
else{
    console.log("testFailed")
}