const each = require("../each");
const items = [1, 2, 3, 4, 5, 5];
let testString = ""
let expectedString = "items[0]=1items[1]=2items[2]=3items[3]=4items[4]=5items[5]=5"

function display(number, index) {
    //console.log("items[" + index + "]=" + number)
    testString += "items[" + index + "]=" + number
}

each(items, display)

if (testString == expectedString) {
    console.log("Test Case 1 Passed")
} else {
    console.log("testFailed")
}


//-------------Test case 2 starts here (MDN)----------------
//An object copy function
//The following code creates a copy of a given object.
function copy(obj) {
    const copy = Object.create(Object.getPrototypeOf(obj))
    const propNames = Object.getOwnPropertyNames(obj)

    each(propNames, function (name) {
        const desc = Object.getOwnPropertyDescriptor(obj, name)
        Object.defineProperty(copy, name, desc)
    })

    return copy
}

const obj1 = {
    a: 1,
    b: 2
}
const obj2 = copy(obj1)
if(JSON.stringify(obj1)==JSON.stringify(obj2)){
    console.log("TEST CASE 2 Passed")
}
else{
    console.log("Test Case 2 Failed")
}

//Test Case 3 starts here(MDN)-----------------------------
// Modifying the array during iteration
// The following example logs one, two, four.

let words = ['one', 'two', 'three', 'four']
each(words, function (word) {
    //console.log(word)
    if (word === 'two') {
        words.shift() //'one' will delete from array
    }
}) // one // two // four

if(JSON.stringify(['two', 'three', 'four'])==JSON.stringify(words)){
    console.log("TEST CASE 3 Passed")
}
else{
    console.log("Test Case 3 Failed")
}

//Test Case 4 starts here(MDN) ----------------------------------------
// Flatten an array
// The following example is only here for learning purpose. If you want to flatten an array using built-in methods you can use Array.prototype.flat().

function flatten(arr) {
    const result = []

    each(arr,function (i) {
        if (Array.isArray(i)) {
            result.push(...flatten(i))
        } else {
            result.push(i)
        }
    })

    return result
}

// Usage
const nested = [1, 2, 3, [4, 5, [6, 7], 8, 9]]
if(JSON.stringify([1, 2, 3, 4, 5, 6, 7, 8, 9])==JSON.stringify(flatten(nested))){
    console.log("TEST CASE 4 Passed")
}
else{
    console.log("Test Case 4 Failed")
}
