function reduce(elements, cb, startingValue){
    let index=0
    if(startingValue == undefined){
        startingValue = elements[0]
        index = 1
    }
    for(index; index<elements.length; index++)
    { 
        startingValue = cb(startingValue, elements[index])
    }
    return startingValue
}
module.exports = reduce